const fs = require('fs');
const path = require('path');
const uuid = require('uuid/v4');

const config = JSON.parse(fs.readFileSync(path.join(__dirname, "google_oauth.json"))).web;

const session = require('express-session');
const express = require('express');

//Load GoogleAPI
const googleapi = require('googleapis');
const OAuth2 = googleapi.auth.OAuth2;

const passport = require('passport');
const PassportGoogle = require('passport-google-oauth').OAuth2Strategy;
const goauth2Config = {
    clientID: config.client_id,
    clientSecret: config.client_secret,
    callbackURL: config.redirect_uris[0]
}

//should use a real db to store your tokens and profiles
const db = { }

const scopes = [
    "email",
    "profile",
    //"https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/contacts",
    "https://www.googleapis.com/auth/contacts.readonly"
]

const goauth2 = new PassportGoogle(goauth2Config,
    function(accessToken, refreshToken, profile, done) {
        //Should save these in the database
        db.accessToken = accessToken;
        db.refreshToken = refreshToken;
        db.current = profile.email;
        db.profile = profile;
        done(null, profile.emails[0].value);
    });

passport.use(goauth2);
passport.serializeUser(function(email, done) {
    done(null, email);
})
passport.deserializeUser(function(email, done) {
    done(null, email);
})

const app = express();

app.use(session({
    secret: "123456",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.get("/authorize", passport.authenticate("google", {
    scope: scopes,
    accessType: "offline"
}));

app.get("/oauth_callback", passport.authenticate("google", {
    failureRedirect: "/error.html",
    successReturnToOrRedirect: '/contacts'
}));

app.get("/contacts", function(req, resp) {
    //Create a oauth2client
    const oauth2Client = new OAuth2(config.clientID, config.clientSecret, config.callbackURL);
    //Set the access token that we got
    oauth2Client.credentials = {
        access_token: db.accessToken,
        refresh_token: db.refreshToken
    };

    /*
        requestMask.includeField: its person.<one of the attributes below>
     addresses * ageRanges * biographies * birthdays * braggingRights * coverPhotos * emailAddresses * events * genders *
     imClients * locales * memberships * metadata * names * nicknames * occupations * organizations * phoneNumbers *
     photos * relations * relationshipInterests * relationshipStatuses * residences * skills * taglines * urls
     */

    const listConfig = {
        resourceName: 'people/me',
        "requestMask.includeField": "person.names,person.photos,person.emailAddresses" //no spaces
    };
    const peopleAPI = googleapi.people({ version: "v1", auth: oauth2Client});
    peopleAPI.people.connections.list(listConfig,
        function(error, result, param2) {
            if (error)
                resp.status(400).json({success: false, error: error});
            else
                resp.status(200).json({success: true, connections: result.connections });
        })
})

app.use(express.static(path.join(__dirname, "public")));

app.use(function (req, resp) {
    resp.status(404).sendFile(path.join(__dirname, "public/404.html"));
})

app.listen(3000, function() {
    console.info("Application started on port 3000");
})
